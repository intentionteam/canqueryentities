import wordParser from './wordParser.js';

function createParentStruct() {
    return {
        valueSet: null,
        branch: {
            'integer': {
                check: function (word, targetClass) {
                    const nword = Number(word);
                    if (isNaN(nword)) return false;
                    if ((nword ^ 0) != nword) return false;
                    if (targetClass == null) return true;
                    const lower = targetClass.lowerLimit;
                    if ((lower != null) && (nword < lower)) return false;
                    const upper = targetClass.upperLimit;
                    if ((upper != null) && (nword > upper)) return false;
                    return true;
                },
                name: {
                    general: 'integer',
                    en: 'integer',
                    ru: 'целое'
                }
            }
        },
    };
}

function addClass(root, branch, path) {
    path = path.toLowerCase();
    const base = root.branch[path];
    if (base == null) throw new Error(`The base class ${path} is not found in the root`);
    if (branch.branch == null) branch.branch = {};
    if (branch.branch[path] == null) branch.branch[path] = {};
    const br = branch.branch[path];
    if (br.branch == null) br.branch = {};
    return br;
}

function extendClass(root, cls) {
    const ext = cls.extends;
    if (ext == null) throw new Error('The class must have extends field');
    const parent = root.branch[ext];
    if (parent == null) throw new Error('The parent class is not found');
    if (parent.check == null) throw new Error('The parent class does not has check function');
    cls.check = function (word) {
        return parent.check(word, cls);
    };

    const className = wordParser.getClassName(cls.name);
    if (className == null) throw new Error('The class must have a name');
    if (root.branch[className] != null) throw new Error(`The class ${ className } is already exists`);
    root.branch[className] = {...cls};

}

function addClassFromPath(root, cls) {
    const className = wordParser.getClassName(cls.name);
    if (className == null) throw new Error('The class must have a name');
    if (cls.key == null) throw new Error('The class must have an unique key');
    const paths = wordParser.splitWords(cls.path);
    if ((paths == null) || (paths.length == 0)) throw new Error(`path ${ cls.path } is wrong`);
    let branch = root;
    for (let path of paths) {
        branch = addClass(root, branch, path);
    }
    if (branch.valueSet == null) branch.valueSet = {};
    if (branch.valueSet[cls.key] != null) throw new Error('The class is already exists');
    branch.valueSet[cls.key] = cls;
}

function getClassesFromWord(root, word) {
    const res = [];
    for (let key in root.branch) {
        if (!root.branch.hasOwnProperty(key)) continue;
        const cls = root.branch[key];
        if (cls.check(word))
            res.push(cls);
    }
    return res;
}

function detectClasses(root, words) {
    const res = [];
    for (let i = 0; i < words.length; i++) {
        const word = words[i];
        const classes = getClassesFromWord(root, word);
        if (classes.length > 0)
            res.push({
                word,
                classes,
                wordIndex: i
            });
    }
    return res;
}

function getChildClasses(results, sClass, tClasses) {
    if (sClass.branch == null) return results;
    const classes = tClasses.classes;
    for (let cls of classes) {
        const tname = wordParser.getClassName(cls.name);
        if (sClass.branch[tname] == null) continue;
        results.classes.push(sClass.branch[tname]);
    }
    return results;
}

function getChild(sClasses, tClasses) {
    const classes = sClasses.classes;
    const res = { classes: [], word: tClasses.word, wordIndex: tClasses.wordIndex };
    for (let cls of classes) {
        getChildClasses(res, cls, tClasses);
    }
    return res;
}

function getValueSet(results, root, path) {
    for (let cls of root.classes) {
        if (cls.valueSet != null)
            results.push({ path: path.join(' '), valueSet: cls.valueSet, word: root.word, wordIndex: root.wordIndex});
    }
    return results;
}

function linkClassItem(results, classes, index) {
    let root = classes[index];
    const path = [root.word];
    getValueSet(results, root, path);
    for (let i = index + 1; i < classes.length; i++) {
        const tcls = classes[i];
        const tchild = getChild(root, tcls);
        if (tchild.classes.length == 0) return results;
        root = tchild;
        path.push(tchild.word);
        getValueSet(results, root, path);
    }
    return results;
}


function linkClasses(classes) {
    const linked = [];
    for (let i = 0; i < classes.length; i++) {
        linkClassItem(linked, classes, i);
    }
    return linked;
}

function appendClass(tree, cls) {
    const root = tree.root;
    if (cls.extends != null) {
        extendClass(root, cls);
        return;
    }
    addClassFromPath(root, cls);
}



export default class ClassTree {
    constructor() {
        this.root = createParentStruct();
    }
    add(classes) {
        if (!Array.isArray(classes)) {
            appendClass(this, classes);
            return;
        }
        for (let cls of classes) {
            appendClass(this, cls);
        }
    }

    search(text) {
        const words = wordParser.splitWords(text);
        let root = this.root;
        const classes = detectClasses(root, words);
        const linked = linkClasses(classes);
        return {
            data: linked,
            text: text,
            words: words
        }
    }
};