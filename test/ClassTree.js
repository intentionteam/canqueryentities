import ClassTree from'../ClassTree.js';
import assert from 'assert';
import IntentionStorage from 'intention-storage';

const classTree = new ClassTree();
const pKey = IntentionStorage.generateUUID();
const aKey = IntentionStorage.generateUUID();

const ipAddressEntity = [
    {
        extends: 'integer',
        name: {
            general: 'int8',
            en: 'Eight bit integer',
            ru: 'Восьмибитное целое'
        },
        lowerLimit: 0,
        upperLimit: 255,
        key: IntentionStorage.generateUUID()
    }, {
        extends: 'integer',
        name: {
            general: 'int16',
            en: 'Sixteen bit integer',
            ru: 'Шестнадцатибитное целое'
        },
        lowerLimit: 0,
        upperLimit: 65535,
        key: IntentionStorage.generateUUID()
    }, {
        path: 'int16',
        name: {
            general: 'ipport',
            en: 'IP Port',
            ru: 'Интернет порт'
        },
        key: pKey
    }, {
        path: 'int8 int8 int8 int8',
        name: {
            general: 'ipaddress',
            en: 'IP Address',
            ru: 'Интернет адрес'
        },
        key: aKey
    }
];

describe('ClassTree', function() {
    describe('Add', function() {
        it('adds ip address', function () {
            classTree.add(ipAddressEntity);
            const byte = classTree.root.branch.int8;
            assert.ok(byte);
            const word = classTree.root.branch.int8;
            assert.ok(word);
            const port = classTree.root.branch.int16.valueSet[pKey];
            assert.strictEqual(port.name.general, 'ipport');
            const address = classTree.root.branch.int8.branch.int8.branch.int8.branch.int8.valueSet[aKey];
            assert.strictEqual(address.name.general, 'ipaddress');
        });
    });

    describe('Search', function() {
        it('get IPAddress', function () {
            const entities = classTree.search('Зайди на 192 168 0 1 8080');
            const ipObj = entities.data.find(e => e.path == '192 168 0 1');
            const ipAddress = Object.values(ipObj.valueSet)[0];
            assert.strictEqual(ipAddress.name.general, 'ipaddress');
            const portObj = entities.data.find(e => e.path == '8080');
            const ipPort = Object.values(portObj.valueSet)[0];
            assert.strictEqual(ipPort.name.general, 'ipport');
        });
    });

});