import wordParser from '../wordParser.js';
import assert from 'assert';

describe('wordParser', function() {
    describe('Remove punctuation', function() {
        it('Remove comma from sentence', function() {
            const words = wordParser.splitWords('Уважаемая консоль не могла бы ты добавить, пожалуйста, хранилище локалхост');
            assert.strictEqual(words.length, 10);
            const str = words.join(' ');
            assert.strictEqual(str, 'уважаемая консоль не могла бы ты добавить пожалуйста хранилище локалхост')
        });
    });
});