import assert from 'assert';
import main from '../main.js';
import IS from 'intention-storage';

const pKey = IS.generateUUID();
const aKey = IS.generateUUID();

const gAddresses = [
    {
        type: 'type',
        name: {
            general: 'WebAddress',
            en: 'Web address',
            ru: 'Веб адрес'
        },
        words: [{
            ru: 'локалхост',
            en: 'localhost'
        }, {
            ru: 'локал хост',
            en: 'local host',
        }],
        value: 'localhost'
    }, {
        words: {
            ru: 'праздник крови акт 1'
        },
        songs: [{
            name: 'test1',
        }],
        type: 'type',
        value: 'праздник крови акт 1',
        kind: 'artist',
        name: {
            general: 'Music',
            en: 'Music',
            ru: 'Музыка'
        }
    }, {
        words: {
            ru: 'праздник крови акт 2'
        },
        songs: [{
            name: 'test2'
        }],
        type: 'type',
        value: 'праздник крови акт 2',
        kind: 'artist',
        name: {
            general: 'Music',
            en: 'Music',
            ru: 'Музыка'
        }
    }, {
     type: 'class',
        extends: 'integer',
        name: {
            general: 'int8',
            en: 'Eight bit integer',
            ru: 'Восьмибитное целое'
        },
        lowerLimit: 0,
        upperLimit: 255,
        key: IS.generateUUID()
    }, {
        type: 'class',
        extends: 'integer',
        name: {
            general: 'int16',
            en: 'Sixteen bit integer',
            ru: 'Шестнадцатибитное целое'
        },
        lowerLimit: 0,
        upperLimit: 65535,
        key: IS.generateUUID()
    }, {
        type: 'class',
        path: 'int16',
        name: {
            general: 'IPPort',
            en: 'IP Port',
            ru: 'Интернет порт'
        },
        key: pKey
    }, {
        type: 'class',
        path: 'int8 int8 int8 int8',
        name: {
            general: 'IPAddress',
            en: 'IP Address',
            ru: 'Интернет адрес'
        },
        key: aKey
    }
];


describe('Add and query entities', function() {
    let targetAccept = null;
    let target = null;
    let source = null;
    let intentionStorage = null;
    let cError = null;
    let iQuery = null;
    let queryData = null;
    this.timeout(0);

    describe('Create intention for adding entities', function() {
        it ('Create storage', function () {
            intentionStorage = new IS.IntentionStorage();
        });

        it('Should init', function() {
            main.init(intentionStorage);
        });

        it ('should enable stats', function () {
            intentionStorage.statsInterval = 500;
        });

        it ('Set dispatch interval', function () {
            intentionStorage.dispatchInterval = 500;
        });


        it('Should create intention', function() {
            source = intentionStorage.intentions.byKey('EntitiesInfo - None');
            assert.ok(source != null, 'intention must be exists');
        });

        it('create counter intention', function () {
            target = intentionStorage.createIntention({
                title: 'test counter intention',
                input: 'None',
                output: 'EntitiesInfo',
                onData: async function (status, intention, value) {
                    if (status == 'accepted') {
                        targetAccept = {
                            intention: intention,
                            value: value
                        };
                        intention.send('data', this, gAddresses);
                    }
                    if (status == 'error') {
                        cError = value;
                    }

                }
            });
            assert.ok(target != null, 'source must be created');
            const intention = intentionStorage.intentions.byKey(target.getKey());
            assert.ok(intention != null, 'Target must be exists in storage');
        });

        it('target must be accepted', function (done) {
            setTimeout(() => {
                assert.strictEqual(targetAccept.intention.getKey(), 'EntitiesInfo - None');
                assert.strictEqual(cError, null);
                done()
            }, 1000);
        });
    });

    describe('WebAddress', function () {
        it('WebAddress entities must be exists', function () {
            const wordTree = main.wordTree;
            assert.ok(wordTree.root.valueSet == null, 'Word Tree root must not be null');
            const branch = wordTree.root.branch;
            const localhostR = branch['локалхост'];
            const vl = Object.values(localhostR.valueSet)[0];
            assert.strictEqual(vl.value, 'localhost');
            const localR = branch['локал'];
            assert.ok(localR != null, 'local branch must exists');
            const hostR = localR.branch['хост'];
            assert.ok(hostR != null, 'host branch must exists');
            const hl = Object.values(hostR.valueSet)[0];
            assert.strictEqual(hl.value, 'localhost');

            const localhost = branch['localhost'];
            const ll = Object.values(localhost.valueSet)[0];
            assert.strictEqual(ll.value, 'localhost');
            const local = branch['local'];
            assert.ok(local != null, 'local branch must exists');
            const host = local.branch['host'];
            assert.ok(host != null, 'host branch must exists');
            const hh = Object.values(host.valueSet)[0];
            assert.strictEqual(hh.value, 'localhost');
        })
    });

    describe('IPAddress class', function () {
        it('IPAddess class must be exists', function () {
            const classTree = main.classTree;
            const int8 = classTree.root.branch.int8;
            assert.ok(int8);
            const int16 = classTree.root.branch.int16;
            assert.ok(int16);
            const integer = classTree.root.branch.integer;
            assert.ok(integer);
            const ipObj = classTree.root.branch.int8.branch.int8.branch.int8.branch.int8;
            const ipAddress = Object.values(ipObj.valueSet)[0];
            assert.strictEqual(ipAddress.name.general, 'IPAddress');
        })
    });

    describe('Create intention for querying entities', function () {
        it('Create intention', function (done) {
            iQuery = intentionStorage.createIntention({
                title: 'test query intention',
                input: 'Entities',
                output: 'Recognition',
                onData: async function (status, intention, value) {
                    if (status == 'accepted') {
                        assert.strictEqual(intention.getKey(), 'Recognition - Entities');
                        done();
                    }

                    if (status == 'data') {
                        queryData = value;
                    }
                }
            });
            assert.ok(iQuery != null, 'Query intention must be created');
            const intention = intentionStorage.intentions.byKey(iQuery.getKey());
            assert.ok(intention != null, 'Query intention must be exists in storage');
        });
    });

    describe('Query entities for text', function () {
       it('Query', function () {
           iQuery.accepted.send({ text: 'localhost 192 168 0 1 10010'});
       });
       it('Check answer', function (done) {
            setTimeout(() => {
                assert.ok(queryData);
                const [entities, classes] = queryData;
                const wa = entities.data[0];
                const ia = classes.data[1];
                const ip = classes.data[5];
                assert.strictEqual(wa.name.general, 'WebAddress');
                typeof assert(wa.path, 'string');
                assert.strictEqual(ia.name.general, 'IPAddress');
                assert.strictEqual(ip.name.general, 'IPPort');
                assert.strictEqual(wa.value, 'localhost');
                assert.strictEqual(ia.value, '192 168 0 1');
                typeof assert(ia.path, 'string');
                assert.strictEqual(ip.value, '10010');
                done();
            }, 500);
        });
    });

    describe('Entities same by words but different by position must exists', function () {
        it('Query', function () {
            iQuery.accepted.send({ text: 'Добавить хранилище 192.168.0.110:192'});
        });

        it('Check answer', function (done) {
            setTimeout(() => {
                assert.ok(queryData);
                assert.strictEqual(queryData.length, 1);
                const classes = queryData[0].data;
                assert.strictEqual(classes.length, 7);
                const fport =  classes[6];
                assert.strictEqual(fport.name.general, 'IPPort');
                done();
            }, 500);
        });
    });

    describe('Disable stats', function () {
        it('should disable stats', function () {
            intentionStorage.statsInterval = 0;
        });

        it('disable dispatch interval', function () {
            intentionStorage.dispatchInterval = 0;
        });
    });
});