import WordTree from '../WordTree.js';
import assert from 'assert';
import IntentionStorage from 'intention-storage';

const wordTree = new WordTree();

const waName = {
    name: 'WebAddress',
    en: 'Web address',
    ru: 'Веб адрес'
};

const key = IntentionStorage.generateUUID();
const taskKey = IntentionStorage.generateUUID();

describe('WordTree', function() {
    describe('Add', function() {
        it('adds single word web address', function() {
            wordTree.add('локалхост',
                {
                    key: key,
                    type: 'type',
                    name: waName,
                    words: {
                        ru: 'локалхост',
                        en: 'localhost'
                    },
                    value: 'localhost'
                },
            );
            assert.ok(wordTree.root.valueSet == null, 'Word Tree root must not be null');
            const branch = wordTree.root.branch;
            const localhost = branch['локалхост'];
            const lv = localhost.valueSet[key];
            assert.strictEqual(lv.value, 'localhost');
        });
        it('adds double word web address', function() {
            wordTree.add('локал хост',
                {
                    key: key,
                    type: 'type',
                    name: waName,
                    words: {
                        ru: 'локал хост',
                        en: 'local host',
                    },
                    value: 'localhost'
                }
            );
            assert.ok(wordTree.root.valueSet == null, 'Word Tree root must not be null');
            const branch = wordTree.root.branch;
            const local = branch['локал'];
            assert.ok(local != null, 'local branch must exists');
            const host = local.branch['хост'];
            const lv = host.valueSet[key];
            assert.ok(host != null, 'host branch must exists');
            assert.strictEqual(lv.value, 'localhost');
        });
        it('adds word array web address', function() {
            wordTree.add(['local host', 'localhost'],
                {
                    key: key,
                    type: 'type',
                    name: waName,
                    words: [{
                        ru: 'локал хост',
                        en: 'local host',
                    }],
                    value: 'localhost'
                }
            );
            const branch = wordTree.root.branch;
            const localhost = branch['localhost'];
            assert.strictEqual(localhost.valueSet[key].value, 'localhost');
            const local = branch['local'];
            assert.ok(local != null, 'local branch must exists');
            const host = local.branch['host'];
            assert.ok(host != null, 'host branch must exists');
            assert.strictEqual(host.valueSet[key].value, 'localhost');
        });
        it('adds task', function() {
            wordTree.add(['Добавить хранилище', 'Add storage'], {
                type: 'task',
                key: taskKey,
                name: {
                    name: 'Add storage',
                    en: 'Add storage',
                    ru: 'Добавление хранилища'
                },
                words: {
                        ru: 'Добавить хранилище',
                        en: 'Add storage'
                    },
                    parameters: [{
                        name: 'webAddress',
                        ru: 'веб адрес?',
                        en: 'web address?'
                    }],
                    intentions: [{
                        title: 'Add linked storage task',
                        input: 'StorageOperationInfo',
                        output: 'StorageInfo'
                    }]
                }
            );
        });
        it('adds music entity', function() {
            wordTree.add(['праздник крови акт 1'],{
                key: IntentionStorage.generateUUID(),
                words: {
                    ru: 'праздник крови акт 1'
                },
                songs: [{
                    name: 'test1',
                }],
                type: 'type',
                value: 'праздник крови акт 1',
                kind: 'artist',
                name: {
                    name: 'Music',
                        en: 'Music',
                        ru: 'Музыка'
                }
            });
            wordTree.add(['праздник крови акт 2'],{
                key: IntentionStorage.generateUUID(),
                words: {
                    ru: 'праздник крови акт 2'
                },
                songs: [{
                    name: 'test2'
                }],
                type: 'type',
                value: 'праздник крови акт 2',
                kind: 'artist',
                name: {
                    name: 'Music',
                    en: 'Music',
                    ru: 'Музыка'
                }
            });
            const target = wordTree.root.branch['праздник'].branch['крови'].branch['акт'];
            assert.notStrictEqual(target, null);

        });
        it('searches', async function() {
            const entities = await wordTree.search('Добавь хранилище localhost');
            assert.notStrictEqual(entities, null);
        });
    });
});