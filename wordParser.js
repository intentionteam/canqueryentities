let azp = null;

function loadAZ() {    
    if (azp == null) {
        azp = new Promise((resolve, reject) => {
            const timeout = setTimeout(() => {
                return reject(new Error('Az load time is out'));
            }, 10000);

            const head = window.document.querySelector('head');
            const script = document.createElement('script');
            script.type = 'text/javascript';
            script.id = 'az';
            script.src = '/node_modules/az/dist/az.min.js';       
            script.onload = async () => {
                clearTimeout(timeout);
                await loadParser(window.Az);
                return resolve(window.Az);
            };    
            head.appendChild(script);        
        });        
    }
    return azp;        
}

function loadParser(az) {
    return new Promise((resolve, reject) => {
        az.Morph.init(getDictsPath(), function(err, Morph) {
            if (err == null) return resolve(Morph);
            return reject(err);
        });
    });
}


async function getAz() {
    try {
        if (window) 
            return await loadAZ();
        throw "Az import error";
    } catch (e) {
        const azm = await import('az');
        const az = azm.default;
        await loadParser(az);
        return az;        
    }
}

function getDictsPath() {
    try {
        return (window) ? '/node_modules/az/dicts' : 'node_modules/az/dicts';
    } catch (e) {
        return 'node_modules/az/dicts';
    }
}

async function normalize(word) {
    const Az = await getAz();
    const parses = Az.Morph(word);
    return (parses.length > 0) ? parses[0].normalize().toString() : word;
}

function splitWords(text) {
    const words = text.split(/[\s,.:?!";'`$()]+/);
    const res = [];
    for (let word of words) {
        if (word.length < 1) continue;
        res.push(word.toLowerCase());
    }
    return res;
}


function getClassName(className) {
    if (className.general != null) return className.general;
    if (className.name != null) return className.name;
    return className.toLowerCase();
}

export default {
    normalize, splitWords, getClassName
};
