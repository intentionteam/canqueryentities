import WordTree from './WordTree.js';
import ClassTree from './ClassTree.js';

const gEntities = new WordTree();
const gClasses = new ClassTree();

async function getStorage() {
    try {
        if (window) 
            return (await import('/node_modules/intention-storage/main.js')).default;
        throw "Intention storage import error";
    } catch (e) {
        return (await import('intention-storage')).default;
    }
}

function getEntityKeys(words) {
    if (words == null) throw new Error('Entity must has a words');
    if (typeof(words) == 'string') return [words.toLowerCase()];
    if (typeof(words) == 'object') return Object.values(words).map(v => v.toLowerCase());
    throw new Error('Entity word format is unsupported');
}

function formatEntities(vals) {
    const resMap = {};
    const values = vals.data;
    for (let val of values) {
        const valSet = Object.values(val.valueSet);
        for (let vs of valSet) {
            let pData = resMap[vs.key];
            if (pData == null) {
                pData = vs;
                resMap[vs.key] = Object.assign({ level: val.level, path: val.words.join(' '), wordIndex: val.wordIndex }, pData);
            }
        }
    }
    const rObj = Object.assign({}, vals);
    rObj.data = Object.values(resMap);
    return rObj;
}

function formatClasses(vals) {
    const resMap = {};
    const values = vals.data;
    for (let val of values) {
        for (let key in val.valueSet) {
            if (!val.valueSet.hasOwnProperty(key)) continue;
            const pKey = `${val.path} ${key} ${val.wordIndex}`;
            let pData = resMap[pKey];
            if (pData == null) {
                pData = val.valueSet[key];
                resMap[pKey] = Object.assign({ wordIndex: val.wordIndex, value: val.path }, pData);
            }
        }
    }
    const rObj = Object.assign({}, vals);
    rObj.data = Object.values(resMap);
    return rObj;

}

async function searchEntities(recognition) {
    const res = [];
    if (typeof(recognition) == 'string') {
        const vals = await gEntities.search(recognition);
        if (vals.data.length > 0)
            res.push(formatEntities(vals));
        const cls = await gClasses.search(recognition);
        if (cls.data.length > 0)
            res.push(formatClasses(cls));
    }

    if (recognition.text != null) {
        const vals = await gEntities.search(recognition.text);
        if (vals.data.length > 0)
            res.push(formatEntities(vals));
        const cls = await gClasses.search(recognition.text);
        if (cls.data.length > 0)
            res.push(formatClasses(cls));
    }

    if (recognition.alternatives != null) {
        for (let alt of recognition.alternatives) {
            const vals = await gEntities.search(alt.transcript);
            if (vals.data.length > 0)
                res.push(formatEntities(vals));
            const cls = await gClasses.search(alt.transcript);
            if (cls.data.length > 0)
                res.push(formatClasses(cls));
        }
    }
    return res;
}

function addEntity(entity) {
    const words = Array.isArray(entity.words) ? entity.words : [entity.words];
    for (const word of words) {
        const keys = getEntityKeys(word);
        for (const key of keys) {
            gEntities.add(key, entity);
        }
    }
}

function addClass(entity) {
    gClasses.add(entity);
}

async function addEntities(entities) {
    const IS = await getStorage();
    for (let entity of entities) {
        if (entity.key == null)
            entity.key = IS.generateUUID();
        if (entity.type == null) throw new Error(`Entity ${entity.name.general} must have a type`);
        if (entity.type == 'task') {
            addEntity(entity);
            continue;
        }
        if (entity.type == 'type') {
            addEntity(entity);
            continue;
        }
        if (entity.type == 'class') {
            addClass(entity);
            continue;
        }
        throw new Error(`Entity ${ entity.name.general } has unknown type ${ entity.type }`);
    }
}

function init(intentionStorage) {
    intentionStorage.createIntention({
        title: {
            en: 'Collects information about entities',
            ru: 'Собираю информацию о сущностях'
        },
        input: 'EntitiesInfo',
        output: 'None',
        onData: async function (status, intention, value) {
            if (status == 'data') {
                try {
                    await addEntities(value);
                } catch (e) {
                    intention.send('error', this, {
                        message: e.message
                    });
                }
            }
        }
    });

    intentionStorage.createIntention({
        title: {
            en: 'Can detect known entites from raw user input',
            ru: 'Определяю известные сущности из пользовательского ввода'
        },
        description: {
            en: 'Send me text string and i return you entities from it',
            ru: 'Отправьте мне строку с текстом и я верну вам известные сущности из нее'
        },
        input: 'Recognition',
        output: 'Entities',
        onData: async function (status, intention, value) {
            if (status == 'data') {
                setTimeout(() => {
                    searchEntities(value).then((sr) => {
                        if (sr.length > 0)
                            intention.send('data', this, sr);
                    });
                }, 0);
            }
        }
    });
}

export default {
    wordTree: gEntities,
    classTree: gClasses,
    init: init
};