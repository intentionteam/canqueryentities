import wordParser from './wordParser.js';

function createBranch(valueSet) {
    return {
        valueSet: valueSet,
        branch: {}
    }
}

async function recursiveSearch(root, words, res, current, level = 0) {
    for (let i = 0; i < words.length; i++) {
        const word = await wordParser.normalize(words[i]);
        const branch = root.branch[word];
        if (branch == null) continue;
        const cw = (current == null) ? { words: [], valueSet: null } : current;
        cw.words.push(word);
        cw.valueSet = branch.valueSet;
        let item = null;
        if (cw.valueSet != null) {
            item = {
                words: cw.words,
                valueSet: cw.valueSet,
                level: level,
                word: word,
                wordIndex: i
            };
            res.push(item);
        }
        const nwords = words.filter(v => v != word);
        if (nwords.length == 0) return;
        if (item != null) item.parameters = nwords;
        await recursiveSearch(branch, nwords, res, cw, level + 1);
    }
}

export default class WordTree {
    constructor() {
        this.root = createBranch(null);
    }
    add(text, value) {
        if (value.key == null) throw new Error('The value must have an unique key');
        let atext = (Array.isArray(text)) ? text : [text];
        for (let itext of atext) {
            const words = wordParser.splitWords(itext);
            let root = this.root;
            for (let word of words) {
                if (root.branch[word] == null) root.branch[word] = createBranch(null);
                root = root.branch[word];
            }
            if (root.valueSet == null) root.valueSet = {};
            root.valueSet[value.key] = value;
        }
    }

    async get(text) {
        const words = await wordParser.parseText(text);
        let root = this.root;
        for (let word of words) {
            if (root.branch[word] == null) return null;
            root = root.branch[word];
        }
        return root.valueSet;
    }

    async search(text) {
        const res = [];
        const words = wordParser.splitWords(text);
        await recursiveSearch(this.root, words, res, null, 0);
        return {
            data: res,
            text: text,
            words: words
        };
    }
};